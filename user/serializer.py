from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email','password')

    def create(self, validated_date):
        user = super().create(validated_date)
        user.set_password(validated_date['password'])
        user.save()
        return user
