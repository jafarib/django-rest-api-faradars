from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('myApp.urls')),  # 127.0.0.1:8000/func_name
    path('employee/', include('employee.urls')),  # 127.0.0.1:8000/employee/func_name
    path('car/', include('car.urls')), # 127.0.0.1:8000/car/func_name

    path('api-token-auth', views.obtain_auth_token),
    path('api-auth', include('rest_framework.urls')),
    path('user/',include('user.urls'))
 
]
